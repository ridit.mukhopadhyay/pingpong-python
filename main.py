import pygame
import time
import random



from pygame.locals import (
    K_UP,
    K_DOWN,
    RLEACCEL,
    K_w,
    K_a,
    )

pygame.init()


color_white = (250,250,250)
color_black = (0,0,0)


fps = 120
clock = pygame.time.Clock()
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

game_window = pygame.display.set_mode((SCREEN_WIDTH,SCREEN_HEIGHT))
pygame.display.set_caption("PingPong")


class PlayerPaddle(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("paddle2.jpeg").convert_alpha()
        self.rect = self.image.get_rect(center = (SCREEN_WIDTH-30, SCREEN_HEIGHT/2))
        self.change_y = 0
        
    def update(self):
        if self.rect.y < 0:
            self.rect.y= 0
        elif self.rect.y >= 725:
            self.rect.change_y = 725
        elif self.rect.bottom > SCREEN_HEIGHT:
            self.rect.bottom = SCREEN_HEIGHT
            
        self.change_y = 0
        keystate = pygame.key.get_pressed()
        
        if keystate[K_UP]:
            self.change_y= -20
            
        if keystate[K_DOWN]:
            self.change_y = 20
            
        self.rect.y += self.change_y
        
        if self.rect.top < 0:
            self.change_y *= 0 
            
        if self.rect.bottom < 0:
            self.change_y *= 0
            
class ComputerPaddle(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("paddle2.jpeg").convert_alpha()
        self.rect = self.image.get_rect(center = (10, SCREEN_HEIGHT/2))
        self.change_y = 0
        
    def update(self):
        if self.rect.top < 0: 
            self.rect.top = 0
        if self.rect.bottom > SCREEN_HEIGHT:
            self.rect.bottom = SCREEN_HEIGHT
            
        
        if ball.rect.centery < self.rect.centery:
            self.change_y = -1
            
        if ball.rect.centery > self.rect.centery:
            self.change_y = 1
            
        self.rect.y += self.change_y
        
    
            
class Ball(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("ball.png").convert_alpha()
        self.rect = self.image.get_rect(center=(SCREEN_WIDTH/2,SCREEN_HEIGHT/2))
        self.change_x = random.choice([-1,-2,3])
        self.change_y = random.choice([1,1,2])  
        
    def update(self):
        self.rect.x += self.change_x
        self.rect.y += self.change_y
        if self.rect.top < 0:
            self.change_y *= -1
        if self.rect.bottom > SCREEN_HEIGHT:
            self.change_y *= -1
        if self.rect.left < 0:
            self.change_x *= -1
        if self.rect.right > SCREEN_WIDTH:
            self.change_x *= -1
        if self.change_y == 0:
            self.change_y = random.choice([-1,1])
            
        collision = pygame.sprite.spritecollideany(ball, player_paddle_sprite)
        if collision:
            self.change_x = random.choice([-1,-2,-3])
            self.change_y = random.choice([-3,-2,-1])
            
        collisioncom = pygame.sprite.spritecollideany(ball, computer_paddle_sprite)
        if collisioncom:
            self.change_x = random.choice([1,2,3])
            self.change_y = random.choice([-3,-2,-1,1,2,3])
            
            
class Score():
    def __init__(self):
        self.scorevaluecom = 0
        self.scorevalueplayer = 0
        self.scorefont = pygame.font.SysFont("comicsansms",100)
        self.you_win_font = pygame.font.SysFont("comicsansms",50)
        
    def update(self):
        if ((self.scorevaluecom <5) and (self.scorevalueplayer < 5)):
            if ball.rect.left < 0:
                self.scorevalueplayer += 1        
                
            if ball.rect.right > SCREEN_WIDTH:
                self.scorevaluecom += 1
                    
                
            self.scorevaluecomtobepainted = self.scorefont.render(str(self.scorevaluecom),True,color_white,color_black)
            self.scorevalueplayertobepainted = self.scorefont.render(str(self.scorevalueplayer),True,color_white,color_black)
            self.scorevaluecom_win = self.you_win_font.render("COMPUTER WIN",True,color_white,color_black)
            self.scorevalueplayer_win = self.you_win_font.render("YOU WIN",True,color_white,color_black)
        
    def draw(self):
        game_window.blit(self.scorevaluecomtobepainted,(30,SCREEN_HEIGHT/5))
        game_window.blit(self.scorevalueplayertobepainted,(SCREEN_WIDTH -90,SCREEN_HEIGHT/5))
        if self.scorevaluecom == 5:
            game_window.blit(self.scorevaluecom_win,(SCREEN_WIDTH/2-90, SCREEN_HEIGHT/5))
            ball.kill()
            
        if self.scorevalueplayer == 5:
            game_window.blit(self.scorevalueplayer_win,(SCREEN_WIDTH/2-50, SCREEN_HEIGHT/5))
            ball.kill()
            
                
player_paddle_sprite = pygame.sprite.GroupSingle()
ball_sprite = pygame.sprite.GroupSingle()
computer_paddle_sprite = pygame.sprite.GroupSingle()


playerpaddle = PlayerPaddle()
computerpaddle = ComputerPaddle()
ball = Ball()
score=Score()
player_paddle_sprite.add(playerpaddle)
computer_paddle_sprite.add(computerpaddle)
ball_sprite.add(ball)
        
        


running = True
while running:
    clock.tick(fps)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
            pygame.quit()
    
    #if ((score.scorevaluecom <5) and (score.scorevalueplayer<5)):
    ball_sprite.update()      
    player_paddle_sprite.update()
    computer_paddle_sprite.update()
    score.update()
    game_window.fill(color_black)
    score.draw()
    ball_sprite.draw(game_window)
    computer_paddle_sprite.draw(game_window)
    player_paddle_sprite.draw(game_window)
    pygame.display.flip()
            
    
    
    